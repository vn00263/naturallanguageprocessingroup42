
from locust import HttpUser, task, between

class WebsiteUser(HttpUser):
    wait_time = between(1, 2)  # Wait time between tasks

    @task
    def predict(self):
        large_text = """
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non leo vel dolor consequat interdum. 
        Curabitur aliquam luctus ex, in eleifend arcu facilisis at. Proin venenatis mi at nunc efficitur, 
        sed hendrerit purus auctor. Maecenas eu risus orci. Curabitur imperdiet, sapien at sagittis finibus, 
        erat est tempus turpis, ac ultricies lorem lectus id nisl. Quisque vehicula nisl ac tincidunt cursus. 
        Aenean blandit, felis et vulputate tempus, leo lorem malesuada libero, non dignissim nisi purus ac eros. 
        Integer nec libero dictum, sodales magna sit amet, maximus eros. Aliquam ac malesuada nunc, a malesuada lectus. 
        Nulla eget efficitur lacus. Morbi porttitor, justo ut gravida tempus, purus est ultricies orci, non vulputate 
        elit lectus eu nisl. Sed at orci in est cursus volutpat.

        Phasellus hendrerit libero in ante aliquam, sit amet vulputate risus laoreet. Donec vestibulum dapibus ante, 
        a dictum est facilisis a. Vivamus ut metus nec nisi posuere tincidunt. Etiam convallis turpis non nunc consequat, 
        at bibendum dolor pellentesque. Integer at eros venenatis, convallis sem nec, convallis turpis. Nullam ullamcorper 
        velit nec ante pretium, at ullamcorper leo iaculis. Nullam lacinia odio a metus convallis convallis. 
        Vivamus feugiat, justo vel tincidunt interdum, nisi nisi suscipit dolor, nec ultrices velit libero eget dolor. 
        Nam ac nunc felis. In eget scelerisque lectus, at dignissim magna. Morbi ut ornare felis, sit amet tincidunt sapien. 
        Suspendisse potenti. Nullam auctor turpis ut varius bibendum. Cras quis nisl nisl. Aenean euismod mauris et libero 
        consectetur, a gravida libero malesuada. Sed nec molestie justo. Duis eu odio in purus tincidunt ultrices non eu ex.

        Suspendisse potenti. Ut id sollicitudin leo, vel posuere sapien. Curabitur euismod, nibh non cursus gravida, 
        dolor libero varius lacus, nec bibendum ex mi a nunc. Quisque eu tincidunt risus. Nulla facilisi. Nullam fringilla, 
        orci at faucibus scelerisque, mi odio gravida arcu, in finibus sem magna eget eros. Phasellus egestas nisi sapien, 
        sit amet lacinia arcu laoreet sed. Cras id tortor vitae elit laoreet pellentesque. Donec pulvinar diam et tortor 
        suscipit, ut scelerisque dui ultrices. Nullam consectetur malesuada orci, non feugiat elit vulputate non. 
        Vestibulum vel ultrices velit. Integer ac faucibus nulla.

        Donec ullamcorper libero nec facilisis finibus. Duis ut varius velit. Curabitur pharetra, orci sed condimentum suscipit, 
        risus purus feugiat justo, at posuere magna dolor vel nisl. Nam scelerisque, mauris id aliquam auctor, velit leo 
        cursus risus, eu laoreet odio leo a libero. Praesent bibendum magna ut risus sagittis, ac molestie nisl elementum. 
        In non velit neque. Etiam efficitur lectus eu arcu malesuada fermentum. Donec vel nisi at eros suscipit consequat. 
        Vestibulum at purus vitae dui vehicula pellentesque. Donec ut urna sit amet purus dictum luctus. Donec sit amet 
        convallis mi. Duis a ullamcorper orci. Sed consequat nibh eu tempor vehicula. Nulla facilisi.

        Curabitur gravida eu purus eget auctor. Suspendisse potenti. Nam id sollicitudin libero. Morbi ac est euismod, 
        vehicula ipsum id, bibendum orci. Nulla facilisi. Curabitur eu leo eu lorem elementum cursus. Nulla a tempor 
        tortor. Integer auctor felis magna, et bibendum ipsum dapibus eu. Praesent dictum tincidunt dolor. Vestibulum 
        accumsan eget augue vitae sollicitudin. Cras et libero non lacus pulvinar auctor in non nisl. Phasellus egestas 
        quam ut augue volutpat, sit amet sagittis risus mollis. Curabitur malesuada, risus ut aliquet eleifend, ipsum 
        tortor ultricies turpis, ac tincidunt urna nisi ac urna. Quisque accumsan viverra leo vel gravida.
        """
        
        response = self.client.post("/predict", json={"input": large_text})
        if response.status_code != 200:
            print(f"Request failed with status code {response.status_code}")

if __name__ == "__main__":
    import os
    os.system("locust -f locustfile.py --host=http://127.0.0.1:5000")
    