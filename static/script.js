document.getElementById('ner-form').addEventListener('submit', async function (event) {
    event.preventDefault();
    
    const inputText = document.getElementById('input-text').value;
    const response = await fetch('/predict', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ input: inputText })
    });

    const data = await response.json();
    const predictionsDiv = document.getElementById('predictions');
    
    predictionsDiv.innerHTML = '<h2>Predictions:</h2>';
    data.predictions.forEach((prediction, index) => {
        const div = document.createElement('div');
        div.className = 'prediction-item';
        div.textContent = `Word ${index + 1}: ${prediction}`;
        predictionsDiv.appendChild(div);
    });
});
