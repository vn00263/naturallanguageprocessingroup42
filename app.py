import tensorflow as tf
from flask import Flask, request, jsonify, render_template
import numpy as np
import logging
from logging.handlers import RotatingFileHandler

app = Flask(__name__)

# Load your model using load_model
model_path = 'best_model.h5'  # Update with the actual path to your model
model = tf.keras.models.load_model(model_path)

# Define the label encoding
label_encoding = {"B-O": 0, "B-AC": 1, "B-LF": 2, "I-LF": 3}
reverse_label_encoding = {v: k for k, v in label_encoding.items()}

# Set up logging
log_file_path = 'app.log'
log_handler = RotatingFileHandler(log_file_path, maxBytes=10000, backupCount=1)
log_handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(message)s')
log_handler.setFormatter(formatter)

logger = logging.getLogger('FlaskAppLogger')
logger.setLevel(logging.INFO)
logger.addHandler(log_handler)
logger.info("Logging setup complete.")

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict', methods=['POST'])
def predict():
    data = request.get_json(force=True)
    input_text = data['input']
    logger.info("Received predict request.")  # Log that the request was received
    
    # Preprocess input_text to match model's expected input shape
    processed_input, actual_length = preprocess_input(input_text)
    
    # Make prediction
    predictions = model(processed_input, training=False)
    
    # Convert probabilities to class labels and truncate to actual length
    predicted_classes = np.argmax(predictions.numpy(), axis=-1)[0][:actual_length]
    predicted_labels = [reverse_label_encoding[pred] for pred in predicted_classes]
    
    # Log the interaction
    log_interaction(input_text, predicted_labels)
    
    # Return predictions
    output = {'predictions': predicted_labels}
    return jsonify(output)

def preprocess_input(input_text):
    tokens = input_text.split()
    max_len = 200
    actual_length = len(tokens)
    tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=10000)
    tokenizer.fit_on_texts([input_text])
    sequences = tokenizer.texts_to_sequences([input_text])
    padded_sequences = tf.keras.preprocessing.sequence.pad_sequences(sequences, maxlen=max_len, padding='post', truncating='post')
    return np.array(padded_sequences), actual_length

def log_interaction(input_text, predicted_labels):
    try:
        log_message = f"Input: {input_text}, Predictions: {predicted_labels}"
        logger.info(log_message)
    except Exception as e:
        logger.error(f"Failed to log interaction: {e}")

if __name__ == '__main__':
    # Run the application
    app.run(debug=True)
